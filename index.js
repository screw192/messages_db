const express = require("express");
const fileDb = require("./fileDb");
const messages = require("./app/messages");
const app = express();
app.use(express.json());

fileDb.init();

const port = 8000;

app.use("/messages", messages);

app.listen(port, () => {
  console.log(`Server started on ${port} port`);
});