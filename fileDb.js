const fs = require("fs");
const path = "./messages";

let data = [];

module.exports = {
  init() {
    try {
      fs.readdir(path, (err, files) => {
        files.forEach(file => {
          const fileContents = fs.readFileSync(`${path}/${file}`);
          data.push(JSON.parse(fileContents.toString()));
        });
      });
    } catch (e) {
      data = [];
    }
  },
  getItems() {
    let responseMessages;
    if (data.length > 5) {
      responseMessages = data.slice(-5);
    } else {
      responseMessages = data;
    }

    return responseMessages;
  },
  addMessage(message) {
    const messageDate = new Date().toISOString();
    message.datetime = messageDate;

    data.push(message);
    this.save(messageDate, message);
  },
  save(datetime, messageBody) {
    fs.writeFileSync(`./messages/${datetime}.txt`, JSON.stringify(messageBody, null, 2));
  }
}