const express = require("express");
const fileDb = require("../fileDb");
const router = express.Router();

router.get("/", (req, res) => {
  const messages = fileDb.getItems();
  res.send(messages);
});

router.post("/", (req, res) => {
  fileDb.addMessage(req.body);
  res.send(req.body);
});

module.exports = router;